CREATE TABLE emp (
  empid int PRIMARY KEY auto_increment,
  name VARCHAR(30),
  salary float,
  age int
);