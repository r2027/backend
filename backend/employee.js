const express = require('express')
const db = require('./db')
const utils = require('./utils')

const router = express.Router()

//RMM for adding new employee
router.post('/', (request, response) => {
    const { name,salary,age } = request.body
    const query = `
      INSERT INTO emp
        (name, salary,age)
      VALUES
        ('${name}','${salary}','${age}')
    `
    db.execute(query, (error, result) => {
      response.send(utils.createResult(error, result))
    })
  })

  //RMM for getting all employees
  router.get('/', (request, response) => {
    const query = `
      SELECT *
      FROM emp
    `
    db.execute(query, (error, result) => {
      response.send(utils.createResult(error, result))
    })
  })

//RMM for update employee
router.put('/:id', (request, response) => {
  const { id } = request.params
  const { name,salary,age } = request.body

  const query = `
    UPDATE emp
    SET
      name = '${name}', 
      salary = '${salary}',
      age = '${age}'
    WHERE
      empid = ${id}
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})






  //RMM for delete employee
  router.delete('/:id', (request, response) => {
    const { id } = request.params
  
    const query = `
      DELETE FROM emp
      WHERE
        empid = ${id}
    `
    db.execute(query, (error, result) => {
      response.send(utils.createResult(error, result))
    })
  })













  module.exports = router